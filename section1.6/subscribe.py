# Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
# SPDX-License-Identifier: MIT-0

import time as time
import json
from awscrt import mqtt
import AWSIoTPythonSDK.MQTTLib as AWSIoTPyMQTT

# Define ENDPOINT, CLIENT_ID, PATH_TO_CERTIFICATE, PATH_TO_PRIVATE_KEY, PATH_TO_AMAZON_ROOT_CA_1, MESSAGE, TOPIC, and RANGE
ENDPOINT = "a3db26kvgac4er-ats.iot.us-east-2.amazonaws.com"

PATH_TO_AMAZON_ROOT_CA_1 = "root.pem"
MESSAGE = "Hello World"
TOPIC = "test/testing"

def customOnMessage(message):
    pass

# Suback callback
def customCallback(client, userdata, message):
    print("Received a new message")
    print(message.payload)
    print("from topic: ")
    print(message.topic)
    print("--------------\n\n")



print("------------------------------------------------------------------------------------------\n\n")
# device 2 sub

CLIENT_ID = "device_2"
PATH_TO_CERTIFICATE = "device_2/device_2.pem"
PATH_TO_PRIVATE_KEY = "device_2/device_2_private.pem.key"

myAWSIoTMQTTClient = AWSIoTPyMQTT.AWSIoTMQTTClient(CLIENT_ID)
myAWSIoTMQTTClient.configureEndpoint(ENDPOINT, 8883)
myAWSIoTMQTTClient.configureCredentials(PATH_TO_AMAZON_ROOT_CA_1, PATH_TO_PRIVATE_KEY, PATH_TO_CERTIFICATE)
myAWSIoTMQTTClient.onMessage = customOnMessage
myAWSIoTMQTTClient.connect()

# Connect and subscribe to AWS IoT
myAWSIoTMQTTClient.connect()

myAWSIoTMQTTClient.subscribe(TOPIC, 1, customCallback)
time.sleep(2)
input()
myAWSIoTMQTTClient.disconnect()
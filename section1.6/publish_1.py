# Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
# SPDX-License-Identifier: MIT-0

import time as time
import json
from awscrt import mqtt
import AWSIoTPythonSDK.MQTTLib as AWSIoTPyMQTT

# Define ENDPOINT, CLIENT_ID, PATH_TO_CERTIFICATE, PATH_TO_PRIVATE_KEY, PATH_TO_AMAZON_ROOT_CA_1, MESSAGE, TOPIC, and RANGE
ENDPOINT = "a3db26kvgac4er-ats.iot.us-east-2.amazonaws.com"

PATH_TO_AMAZON_ROOT_CA_1 = "root.pem"
MESSAGE = "Hello World"
TOPIC = "test/testing"

def customOnMessage(message):
    #TODO3: fill in the function to show your received message
    print(message.payload)

# Suback callback
def customCallback(client, userdata, message):
    print("Received a new message")
    print(message.payload)
    print("from topic: ")
    print(message.topic)
    print("--------------\n\n")


#device 0 pub
CLIENT_ID = "device_0"
PATH_TO_CERTIFICATE = "device_0/device_0.pem"
PATH_TO_PRIVATE_KEY = "device_0/device_0_private.pem.key"



myAWSIoTMQTTClient = AWSIoTPyMQTT.AWSIoTMQTTClient(CLIENT_ID)
myAWSIoTMQTTClient.configureEndpoint(ENDPOINT, 8883)
myAWSIoTMQTTClient.configureCredentials(PATH_TO_AMAZON_ROOT_CA_1, PATH_TO_PRIVATE_KEY, PATH_TO_CERTIFICATE)
myAWSIoTMQTTClient.onMessage = customOnMessage
myAWSIoTMQTTClient.connect()

# Connect and subscribe to AWS IoT
#myAWSIoTMQTTClient.connect()

#myAWSIoTMQTTClient.subscribe(TOPIC, 1, customCallback)
time.sleep(2)

message = {}
msg = 'Hello world from device 0'
message['message'] = msg
messageJson = json.dumps(message)
print("\n\n")
print('Published topic %s: %s\n' % (TOPIC, messageJson))
myAWSIoTMQTTClient.publish(TOPIC, messageJson, 1)
time.sleep(1)
myAWSIoTMQTTClient.disconnect()

print("------------------------------------------------------------------------------------------\n\n")


# device 1 pub 

CLIENT_ID = "device_1"
PATH_TO_CERTIFICATE = "device_1/device_1.pem"
PATH_TO_PRIVATE_KEY = "device_1/device_1_private.pem.key"

myAWSIoTMQTTClient = AWSIoTPyMQTT.AWSIoTMQTTClient(CLIENT_ID)
myAWSIoTMQTTClient.configureEndpoint(ENDPOINT, 8883)
myAWSIoTMQTTClient.configureCredentials(PATH_TO_AMAZON_ROOT_CA_1, PATH_TO_PRIVATE_KEY, PATH_TO_CERTIFICATE)
myAWSIoTMQTTClient.onMessage = customOnMessage
myAWSIoTMQTTClient.connect()

# Connect and subscribe to AWS IoT
#myAWSIoTMQTTClient.connect()

#myAWSIoTMQTTClient.subscribe(TOPIC, 1, customCallback)
time.sleep(2)

message = {}
msg = 'Hello world from device 1'
message['message'] = msg
messageJson = json.dumps(message)
print("--------------\n\n")
print('Published topic %s: %s\n' % (TOPIC, messageJson))
myAWSIoTMQTTClient.publish(TOPIC, messageJson, 1)
time.sleep(1)
myAWSIoTMQTTClient.disconnect()

import json
import logging
import json
import sys
import greengrasssdk

client = greengrasssdk.client("iot-data")

# Logging
logger = logging.getLogger(__name__)
logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
 

# Counter
my_counter = 0
def lambda_handler(event, context):
    global max_emission

    #TODO1: Get your data
    print("Received event: " + json.dumps(event, indent=2)) 
    msg = json.dumps(event, indent=2)

    if 'status' in event:
        client.publish(
                topic="vehicle/vehicle_0/response",
                qos=1,
                payload=json.dumps(
                    {"message": "Max emission for vehicle 0 is {}".format(max_emission)}
                )
            )
        max_emission = 0
        return
    
    #TODO2: Calculate max CO2 emission
    try:
        max_emission
    except NameError:
        max_emission = 0


    if event['message'] > max_emission:
        max_emission = event['message']

    #TODO3: Return the result
    
    my_counter += 1

    return

# /*
# * Copyright 2010-2017 Amazon.com, Inc. or its affiliates. All Rights Reserved.
# *
# * Licensed under the Apache License, Version 2.0 (the "License").
# * You may not use this file except in compliance with the License.
# * A copy of the License is located at
# *
# *  http://aws.amazon.com/apache2.0
# *
# * or in the "license" file accompanying this file. This file is distributed
# * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
# * express or implied. See the License for the specific language governing
# * permissions and limitations under the License.
# */


import os
import sys
import time
import uuid
import json
import logging

import argparse
from AWSIoTPythonSDK.core.greengrass.discovery.providers import DiscoveryInfoProvider
from AWSIoTPythonSDK.core.protocol.connection.cores import ProgressiveBackOffCore
from AWSIoTPythonSDK.MQTTLib import AWSIoTMQTTClient
from AWSIoTPythonSDK.exception.AWSIoTExceptions import DiscoveryInvalidRequestException


MAX_DISCOVERY_RETRIES = 1
GROUP_CA_PATH = "./groupCA-responder/"
data_path = "vehicle{}.csv"
cert_path = "vehicle_{}_certs"
client_id_path = "vehicle_{}"
topic_path = "vehicle/vehicle_{}"

device_ids = []
device_ids.append('3ae8aa30fdfd711282068d870e87c85c39b7c008bc2c4157fc2a223d1f4d6f87')


# Configure logging
logger = logging.getLogger("AWSIoTPythonSDK.core")
logger.setLevel(logging.DEBUG)
streamHandler = logging.StreamHandler()
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
streamHandler.setFormatter(formatter)
logger.addHandler(streamHandler)

# Progressive back off core
backOffCore = ProgressiveBackOffCore()

# General message notification callback
def customOnMessage(message):
    print('Received message on topic %s: %s\n' % (message.topic, message.payload))

# Read in command-line parameters
host = 'a3db26kvgac4er-ats.iot.us-east-2.amazonaws.com'

rootCAPath = 'vehicle_0_responder/AmazonRootCA1.pem' 
certificatePath = 'vehicle_0_responder/3ae8aa30fdfd711282068d870e87c85c39b7c008bc2c4157fc2a223d1f4d6f87' + '-certificate.pem.crt'
privateKeyPath = 'vehicle_0_responder/3ae8aa30fdfd711282068d870e87c85c39b7c008bc2c4157fc2a223d1f4d6f87' + '-private.pem.key'
clientId = 'vehicle_0_responder'
thingName = 'vehicle_0_responder'
topic = 'vehicle/vehicle_0/response'
print_only = False
# Discover GGCs
discoveryInfoProvider = DiscoveryInfoProvider()
discoveryInfoProvider.configureEndpoint(host)
discoveryInfoProvider.configureCredentials(rootCAPath, certificatePath, privateKeyPath)
discoveryInfoProvider.configureTimeout(10)  # 10 sec

discovered = False
groupCA = None
coreInfo = None
try:
    discoveryInfo = discoveryInfoProvider.discover(thingName)
    caList = discoveryInfo.getAllCas()
    coreList = discoveryInfo.getAllCores()

    # We only pick the first ca and core info
    groupId, ca = caList[0]
    coreInfo = coreList[0]
    print("Discovered GGC: %s from Group: %s" % (coreInfo.coreThingArn, groupId))

    print("Now we persist the connectivity/identity information...")
    groupCA = GROUP_CA_PATH + groupId + "_CA_" + str(uuid.uuid4()) + ".crt"
    if not os.path.exists(GROUP_CA_PATH):
        os.makedirs(GROUP_CA_PATH)
    groupCAFile = open(groupCA, "w")
    groupCAFile.write(ca)
    groupCAFile.close()

    discovered = True
    print("Now proceed to the connecting flow...")
except DiscoveryInvalidRequestException as e:
    print("Invalid discovery request detected!")
    print("Type: %s" % str(type(e)))
    print("Error message: %s" % str(e))
    print("Stopping...")
except BaseException as e:
    print("Error in discovery!")
    print("Type: %s" % str(type(e)))
    print("Error message: %s" % str(e))
    print("Backing off...\n")
    backOffCore.backOff()

print(discovered)
if not discovered:
    # With print_discover_resp_only flag, we only woud like to check if the API get called correctly. 
    if print_only:
        sys.exit(0)
    print("Discovery failed after %d retries. Exiting...\n" % (MAX_DISCOVERY_RETRIES))
    sys.exit(-1)


# Iterate through all connection options for the core and use the first successful one
myAWSIoTMQTTClient = AWSIoTMQTTClient(clientId)
myAWSIoTMQTTClient.configureCredentials(groupCA, privateKeyPath, certificatePath)
myAWSIoTMQTTClient.onMessage = customOnMessage

connected = False
for connectivityInfo in coreInfo.connectivityInfoList:
    currentHost = connectivityInfo.host
    currentPort = connectivityInfo.port
    print("Trying to connect to core at %s:%d" % (currentHost, currentPort))
    myAWSIoTMQTTClient.configureEndpoint(currentHost, currentPort)
    try:
        myAWSIoTMQTTClient.connect()
        connected = True
        break
    except BaseException as e:
        print("Error in connect!")
        print("Type: %s" % str(type(e)))
        print("Error message: %s" % str(e))

if not connected:
    print("Cannot connect to core %s. Exiting..." % coreInfo.coreThingArn)
    sys.exit(-2)


#print("Loading vehicle data...")
#data = pd.read_csv(data_path.format(i))
#maximum_value = data['vehicle_CO2'].max()
#message = {}
#message['message'] = maximum_value
#message['sequence'] = i
#messageJson = json.dumps(message)
myAWSIoTMQTTClient.subscribe(topic, 0, None)
#print('Subscribed topic %s: %s\n' % (topic, messageJson))
time.sleep(2)
while True:
    time.sleep(2)
# /*
# * Copyright 2010-2017 Amazon.com, Inc. or its affiliates. All Rights Reserved.
# *
# * Licensed under the Apache License, Version 2.0 (the "License").
# * You may not use this file except in compliance with the License.
# * A copy of the License is located at
# *
# *  http://aws.amazon.com/apache2.0
# *
# * or in the "license" file accompanying this file. This file is distributed
# * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
# * express or implied. See the License for the specific language governing
# * permissions and limitations under the License.
# */


import os
import sys
import time
import uuid
import json
import logging
import pandas as pd
import numpy as np

import argparse
from AWSIoTPythonSDK.core.greengrass.discovery.providers import DiscoveryInfoProvider
from AWSIoTPythonSDK.core.protocol.connection.cores import ProgressiveBackOffCore
from AWSIoTPythonSDK.MQTTLib import AWSIoTMQTTClient
from AWSIoTPythonSDK.exception.AWSIoTExceptions import DiscoveryInvalidRequestException


MAX_DISCOVERY_RETRIES = 1
GROUP_CA_PATH = "./groupCA{}/"
data_path = "vehicle{}.csv"
cert_path = "vehicle_{}_certs"
client_id_path = "vehicle_{}"
topic_path = "vehicle/vehicle_{}"

device_ids = []
device_ids.append('ab9563b83bf59f6d001ccb1e946e64626bc60de313ab14963eae7ae8ff8de8c8')
device_ids.append('51306efcce8cfdd4f85ef6927775ac2723aafe91544f7ab1ea21d11d16be7fe0')
device_ids.append('836adb6a9e0db32b98a2da828e610597b974f918c42f7aa868d7f1530f89cd23')
device_ids.append('4587e6cf71f722494c257fae75193c3bffeb245bc8d143e82c09fbc74d4b5799')
device_ids.append('f47c058148a387d18b1e6d397ae3895c8ce343ee553a195367503b61341a815d')

# Configure logging
logger = logging.getLogger("AWSIoTPythonSDK.core")
logger.setLevel(logging.DEBUG)
streamHandler = logging.StreamHandler()
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
streamHandler.setFormatter(formatter)
logger.addHandler(streamHandler)

# Progressive back off core
backOffCore = ProgressiveBackOffCore()

# General message notification callback
def customOnMessage(message):
    print('Received message on topic %s: %s\n' % (message.topic, message.payload))

# Read in command-line parameters
host = 'a3db26kvgac4er-ats.iot.us-east-2.amazonaws.com'
for i in range(5):
    rootCAPath = cert_path.format(i) + '/AmazonRootCA1.pem' 
    certificatePath = cert_path.format(i) + '/' + device_ids[i] + '-certificate.pem.crt'
    privateKeyPath = cert_path.format(i) + '/' + device_ids[i] + '-private.pem.key'
    clientId = client_id_path.format(i)
    thingName = client_id_path.format(i)
    topic = topic_path.format(i)
    print_only = False
    # Discover GGCs
    discoveryInfoProvider = DiscoveryInfoProvider()
    discoveryInfoProvider.configureEndpoint(host)
    discoveryInfoProvider.configureCredentials(rootCAPath, certificatePath, privateKeyPath)
    discoveryInfoProvider.configureTimeout(10)  # 10 sec

    discovered = False
    groupCA = None
    coreInfo = None
    try:
        discoveryInfo = discoveryInfoProvider.discover(thingName)
        caList = discoveryInfo.getAllCas()
        coreList = discoveryInfo.getAllCores()

        # We only pick the first ca and core info
        groupId, ca = caList[0]
        coreInfo = coreList[0]
        print("Discovered GGC: %s from Group: %s" % (coreInfo.coreThingArn, groupId))

        print("Now we persist the connectivity/identity information...")
        groupCA = GROUP_CA_PATH.format(i) + groupId + "_CA_" + str(uuid.uuid4()) + ".crt"
        if not os.path.exists(GROUP_CA_PATH.format(i)):
            os.makedirs(GROUP_CA_PATH.format(i))
        groupCAFile = open(groupCA, "w")
        groupCAFile.write(ca)
        groupCAFile.close()

        discovered = True
        print("Now proceed to the connecting flow...")
    except DiscoveryInvalidRequestException as e:
        print("Invalid discovery request detected!")
        print("Type: %s" % str(type(e)))
        print("Error message: %s" % str(e))
        print("Stopping...")
        break
    except BaseException as e:
        print("Error in discovery!")
        print("Type: %s" % str(type(e)))
        print("Error message: %s" % str(e))
        print("Backing off...\n")
        backOffCore.backOff()

    print(discovered)
    if not discovered:
        # With print_discover_resp_only flag, we only woud like to check if the API get called correctly. 
        if print_only:
            sys.exit(0)
        print("Discovery failed after %d retries. Exiting...\n" % (MAX_DISCOVERY_RETRIES))
        sys.exit(-1)

    print("Iterate through all connection options")
    print(clientId)
    # Iterate through all connection options for the core and use the first successful one
    myAWSIoTMQTTClient = AWSIoTMQTTClient(clientId)
    myAWSIoTMQTTClient.configureCredentials(groupCA, privateKeyPath, certificatePath)
    myAWSIoTMQTTClient.onMessage = customOnMessage

    connected = False
    for connectivityInfo in coreInfo.connectivityInfoList:
        currentHost = connectivityInfo.host
        currentPort = connectivityInfo.port
        print("Trying to connect to core at %s:%d" % (currentHost, currentPort))
        myAWSIoTMQTTClient.configureEndpoint(currentHost, currentPort)
        try:
            myAWSIoTMQTTClient.connect()
            connected = True
            break
        except BaseException as e:
            print("Error in connect!")
            print("Type: %s" % str(type(e)))
            print("Error message: %s" % str(e))

    if not connected:
        print("Cannot connect to core %s. Exiting..." % coreInfo.coreThingArn)
        sys.exit(-2)


    print("Loading vehicle data...")
    data = pd.read_csv(data_path.format(i))
    for idx, row in data.iterrows():
        co2_val = row['vehicle_CO2']
        message = {}
        message['message'] = co2_val
        message['sequence'] = i
        messageJson = json.dumps(message)
        myAWSIoTMQTTClient.publish(topic, messageJson, 0)
        print('Published topic %s: %s\n' % (topic, messageJson))
        time.sleep(1)

    message = {}
    message['status'] = 'done'
    message['sequence'] = 0
    messageJson = json.dumps(message)
    myAWSIoTMQTTClient.publish(topic, messageJson, 0)
    time.sleep(2)
    break